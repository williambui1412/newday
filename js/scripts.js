var app = app || {};

var spBreak = 768;

app.init = function () {
  app.clickMenu();
  app.tabJs();
};

app.isMobile = function () {
  return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;
};

app.clickMenu = function () {
  $('.js-btn-menu').click(function () {
    $(this).toggleClass('active');
    $('.js-navigation').toggleClass('active');
  });
};

app.tabJs = function () {
  $('a.js-tab').click(function (event) {
    event.preventDefault();
    $(this)
      .parent()
      .addClass('active');
    $(this)
      .parent()
      .siblings()
      .removeClass('active');
    var tab = $(this).attr('href');
    var tab_content = $(this).attr('data-tab');
    $('.' + tab_content)
      .not(tab)
      .css('display', 'none');
    $(tab).fadeIn('fast');
  });
};

$(function () {
  app.init();
});
